<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>

<head>
    <title>MVC Example Extended</title>
</head>

<body>





<table border="1">
<tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Email</th>
</tr>


<c:forEach var="student" items="${student_list}">

<tr>
    <td>${student.firstName}</td>
    <td>${student.lastName}</td>
    <td>${student.email}</td>

</tr>


</c:forEach>


</table>







</body>

</html>