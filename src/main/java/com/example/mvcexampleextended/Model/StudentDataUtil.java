package com.example.mvcexampleextended.Model;

import com.example.mvcexampleextended.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentDataUtil {

    public static List<Student> getStudent()
    {
        List <Student> students = new ArrayList<Student>();

        students.add(new Student("Ruwan","Sampath","rrr@g.com"));
        students.add(new Student("Ruwan","Sampath","rrr@g.com"));
        students.add(new Student("Ruwan","Sampath","rrr@g.com"));
        students.add(new Student("Ruwan","Sampath","rrr@g.com"));

        return students;
    }

}
